.PHONY: all
all: check

.PHONY: check
check:
	sort -Vc README
	! grep -vP '^https://xkcd.com/[0-9]+/\t\S+(\s\S+)*$$' README

.PHONY: sort
sort:
	sort -V README -o README

# vim:ts=4 sts=4 sw=4 noet
